﻿using System;
using System.Collections.Generic;
using AdventOfCode.Base;

namespace AdventOfCode.Challenges_2017
{
    class Y2017_D3_P1 : AChallenge
    {
        public static Y2017_D3_P1 Build() { return new Y2017_D3_P1(); }

        // the step increment value and operation for each direction
        private static (int StepIncrement, Action<Y2017_D3_State> DoOneStep)[] ActionOrder =
        {
             (1, (Y2017_D3_State state) => { state.CurrentPoint.X++; }), //right
             (0, (Y2017_D3_State state) => { state.CurrentPoint.Y--; }), //up
             (1, (Y2017_D3_State state) => { state.CurrentPoint.X--; }), //left
             (0, (Y2017_D3_State state) => { state.CurrentPoint.Y++; })  //down
        };

        //https://adventofcode.com/2017/day/3
        public override string Solve(string input)
        {
            int Value = int.Parse(input);

            Point InitialPoint;

            // this is going to be the placeholder for our current state
            Y2017_D3_State state = new Y2017_D3_State
            {
                Table = CreateTable(Value, out InitialPoint),
                InitialPoint = InitialPoint,
                CurrentPoint = new Point { X = InitialPoint.X, Y = InitialPoint.Y },
                CurrentDirectionIndex = 0,
                Steps = 0,
                Current = 1,
                ValueToCompare = Value,
                Stop = false
            };

            // run our operations delegates one by one up until we our stop condition is verified
            do
            {
                Move(ActionOrder[state.CurrentDirectionIndex % ActionOrder.Length], state);
            }
            while (!state.Stop);

            return GenerateOutput(state).ToString();
        }

        protected virtual int GenerateOutput(Y2017_D3_State state)
        {
            // manhattam distance calculation
            return Math.Abs(state.InitialPoint.X - state.CurrentPoint.X) + Math.Abs(state.InitialPoint.Y - state.CurrentPoint.Y);
        }

        protected virtual bool CheckStopCondition(Y2017_D3_State state)
        {
            return state.Stop = (state.Current == state.ValueToCompare);
        }

        protected virtual void SetValues(Y2017_D3_State state)
        {
            state.Current += 1;
            state.Table[state.CurrentPoint.X, state.CurrentPoint.Y] = state.Current;
        }

        // private methods
        private int[,] CreateTable(int Value, out Point InitialPoint)
        {
            // our table size is and integer based on our input value, basically the ceiling of its square root
            int TableSize = Convert.ToInt16(Math.Ceiling(Math.Sqrt(Value)));

            // our table
            int[,] Table = new int[TableSize, TableSize];

            // the point where to place the first item (1)
            InitialPoint = new Point { X = TableSize / 2 + TableSize % 2 - 1, Y = TableSize / 2 - TableSize % 2 };

            Table[InitialPoint.X, InitialPoint.Y] = 1;

            return Table;
        }

        private void Move((int StepIncrement, Action<Y2017_D3_State> DoOneStep) config, Y2017_D3_State state)
        {
            // deconstruct our configuration for the specified direction
            var (StepIncrement, DoOneStep) = config;

            // increment the steps
            state.Steps += StepIncrement;

            for (int i = 0; i < state.Steps; ++i)
            {
                // take a step
                DoOneStep(state);
                SetValues(state);

                if (CheckStopCondition(state))
                {
                    break;
                }
            }

            state.CurrentDirectionIndex++;
        }
    }
}

// support classes
public class Point
{
    public int X;
    public int Y;
}

public class Y2017_D3_State
{
    public int[,] Table;
    public Point InitialPoint;
    public Point CurrentPoint;
    public int Steps;
    public int Current;
    public int CurrentDirectionIndex;
    public int ValueToCompare;
    public bool Stop;
};
