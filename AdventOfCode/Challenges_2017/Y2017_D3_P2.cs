﻿namespace AdventOfCode.Challenges_2017
{
    class Y2017_D3_P2 : Y2017_D3_P1
    {
        public new static Y2017_D3_P2 Build() { return new Y2017_D3_P2(); }

        protected override int GenerateOutput(Y2017_D3_State state)
        {
            return state.Current;
        }

        protected override bool CheckStopCondition(Y2017_D3_State state)
        {
            return state.Stop = state.Current > state.ValueToCompare;
        }

        protected override void SetValues(Y2017_D3_State state)
        {
            Point p = state.CurrentPoint;

            int value = 0;

            //TOP ROW
            value += GetValueAtPosition(state.Table, p.X - 1, p.Y - 1);
            value += GetValueAtPosition(state.Table, p.X, p.Y - 1);
            value += GetValueAtPosition(state.Table, p.X + 1, p.Y - 1);

            // LEFT
            value += GetValueAtPosition(state.Table, p.X - 1, p.Y);

            //BOTTOM ROW
            value += GetValueAtPosition(state.Table, p.X - 1, p.Y + 1);
            value += GetValueAtPosition(state.Table, p.X, p.Y + 1);
            value += GetValueAtPosition(state.Table, p.X + 1, p.Y + 1);

            // RIGHT
            value += GetValueAtPosition(state.Table, p.X + 1, p.Y);

            state.Table[state.CurrentPoint.X, state.CurrentPoint.Y] = state.Current = value;
        }

        private int GetValueAtPosition(int[,] table, int x, int y)
        {
            return table[x, y];
        }
    }
}
