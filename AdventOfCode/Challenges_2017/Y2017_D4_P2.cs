﻿using AdventOfCode.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D4_P2 : Y2017_D4_P1
    {
        public new static Y2017_D4_P2 Build() { return new Y2017_D4_P2(); }

        protected override bool DoesNotExist(HashSet<string> set, string item, out string newItem)
        {
            // ordering the chars on the string to avoid having anagrams
            return base.DoesNotExist(set, new string(item.OrderBy(i => i).ToArray()), out newItem);
        }
    }
}
