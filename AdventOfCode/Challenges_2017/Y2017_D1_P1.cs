﻿using AdventOfCode.Base;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D1_P1 : AChallenge
    {
        public static Y2017_D1_P1 Build() { return new Y2017_D1_P1() { Offset = 1 }; }

        public int Offset { get; set; }

        //https://adventofcode.com/2017/day/1
        public override string Solve(string Input)
        {
            int output = 0;

            for (int i = 0; i < Input.Length; ++i)
            {
                output += GetIncrementValue(Input[i], Input[(i + Offset) % Input.Length]);
            }

            return output.ToString();
        }

        private int GetIncrementValue(char c1, char c2)
        {
            return c1 == c2 ? int.Parse(c1.ToString()) : 0;
        }

        protected override string PrepareInput(string input)
        {
            return input.Trim();
        }
    }
}
