﻿using AdventOfCode.Base;
using AdventOfCode.Utils;
using System.Linq;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D2_P1 : AChallenge
    {
        public static Y2017_D2_P1 Build() { return new Y2017_D2_P1(); }

        //https://adventofcode.com/2017/day/2
        public override string Solve(string Input)
        {
            int output = 0;

            var rows = UsefullTools.GetBidimensionalArray<int>(Input, '\n', '\t');

            foreach(var row in rows)
            {
                output += GetIncrementValue(row);
            }

            return output.ToString();
        }

        protected virtual int GetIncrementValue(int[] row)
        {
            return row.Max() - row.Min();
        }

        protected override string PrepareInput(string input)
        {
            return input.Trim();
        }
    }
}
