﻿using System.Linq;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D2_P2 : Y2017_D2_P1
    {
        public new static Y2017_D2_P2 Build() { return new Y2017_D2_P2(); }

        protected override int GetIncrementValue(int[] row)
        {
            int output = 0;
            row = row.OrderByDescending(item=>item).ToArray();

            for (int i= 0; i<row.Length-1; ++i)
            {
                // skip the position on the row that we're at so we don't have to check for equal value and get less iterations
                var result = row.Skip(i+1).FirstOrDefault(item => row[i] % item == 0);

                if (result > 0)
                {
                    output = row[i] / result;
                    break;
                }
            }

            return output;
        }
    }
}
