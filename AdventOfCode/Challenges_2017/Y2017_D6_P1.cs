﻿using AdventOfCode.Base;
using System.Collections.Generic;
using AdventOfCode.Utils;
using System;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D6_P1 : AChallenge
    {
        public static Y2017_D6_P1 Build() { return new Y2017_D6_P1(); }

        //https://adventofcode.com/2017/day/6
        public override string Solve(string Input)
        {
            int[] values = UsefullTools.GetOneDimensionalArray<int>(Input, '\t');

            // a dictionary to keep track of our memory banks and their index of arrival
            Dictionary<string, int> memoryBanks = new Dictionary<string, int>();

            string memoryBank;

            do
            {
                memoryBank = string.Join("-", values);

                if (!memoryBanks.ContainsKey(memoryBank))
                {
                    memoryBanks.Add(memoryBank, memoryBanks.Count);
                    Distribute(values, GetMaxValueFirstIndex(values));
                    continue;
                }

                break;
            }
            while (true);

            return GenerateOutput(memoryBanks, memoryBank).ToString();
        }

        protected virtual int GenerateOutput(Dictionary<string, int> memoryBanks, string lastMemoryBank)
        {
            return memoryBanks.Count;
        }

        private int GetMaxValueFirstIndex(int[] values)
        {
            int index = 0;
            int max = 0;

            for (int i = 0; i < values.Length; ++i)
            {
                if (values[i] > max)
                {
                    max = values[i];
                    index = i;
                }
            }

            return index;
        }

        private void Distribute(int[] values, int index)
        {
            // copy value and make it 0
            int value = values[index];
            values[index] = 0;

            // get the amount to add per jump
            int toAdd = Math.Max(value / values.Length, 1);

            // get the number os jumps
            int jumps = value / toAdd;

            // add the toAdd value for the number of jumps
            for (int i = 0; i < jumps; ++i)
            {
                values[(index + i + 1) % values.Length] += toAdd;
            }
        }

        protected override string PrepareInput(string input)
        {
            return input.TrimEnd();
        }

    }
}
