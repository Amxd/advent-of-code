﻿namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D1_P2 : Y2017_D1_P1
    {
        public new static Y2017_D1_P2 Build() { return new Y2017_D1_P2(); }

        protected override void Init(string Input)
        {
            Offset = Input.Length / 2;
        }
    }
}
