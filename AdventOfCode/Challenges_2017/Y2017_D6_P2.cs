﻿using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D6_P2 : Y2017_D6_P1
    {
        public new static Y2017_D6_P2 Build() { return new Y2017_D6_P2(); }

        protected override int GenerateOutput(Dictionary<string, int> memoryBanks, string lastMemoryBank)
        {
            return memoryBanks.Count - memoryBanks.First(item => item.Key == lastMemoryBank).Value;
        }
    }
}
