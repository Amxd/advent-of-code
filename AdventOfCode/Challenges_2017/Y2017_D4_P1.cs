﻿using AdventOfCode.Base;
using AdventOfCode.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D4_P1 : AChallenge
    {
        public static Y2017_D4_P1 Build() { return new Y2017_D4_P1(); }

        //https://adventofcode.com/2017/day/4
        public override string Solve(string Input)
        {
            int output = 0;
            var rows = UsefullTools.GetBidimensionalArray<string>(Input, '\n', ' ');

            foreach (var row in rows)
            {
                output += GetIncrementValue(row);
            }

            return output.ToString();
        }

        private int GetIncrementValue(string[] row)
        {
            int output = 1;

            HashSet<string> set = new HashSet<string>();

            for(int i=0; i<row.Length; ++i)
            {
                string item = row[i];

                // lets find out if our item exists based on the heuristic, outputing the value because of part 2 of this challenge :)
                if (DoesNotExist(set, item, out item))
                {
                    set.Add(item);
                    continue;
                }

                output = 0;
                break;
            }

            return output;
        }

        protected virtual bool DoesNotExist(HashSet<string> set, string item, out string newItem)
        {
            newItem = item;
            return !set.Contains(item);
        }

        protected override string PrepareInput(string input)
        {
            return input.TrimEnd();
        }
    }
}
