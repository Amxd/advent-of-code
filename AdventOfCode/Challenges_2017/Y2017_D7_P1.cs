﻿using AdventOfCode.Base;
using System.Linq;
using AdventOfCode.Utils;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D7_P1 : AChallenge
    {
        public static string REGEX_EXPRESSION = @"^(\S+)\s+\((\d+)\)(?:\s+\-\>\s+(.+))?$";
        public static Y2017_D7_P1 Build() { return new Y2017_D7_P1(); }

        //https://adventofcode.com/2017/day/7
        public override string Solve(string Input)
        {
            var items = UsefullTools.GetOneDimensionalArray<string>(Input, '\n');

            var programDictionary = items.Select(item => ParseProgram(item)).ToDictionary(item => item.Name, item => item);

            return "";
        }

        public Program ParseProgram(string Input)
        {
            var match = Regex.Match(Input, REGEX_EXPRESSION);

            return new Program()
            {
                Name = match.Groups[1].Value,
                Weight = int.Parse(match.Groups[2].Value),
                Programs = UsefullTools.GetOneDimensionalArray<string>(Regex.Replace(match.Groups[3]?.Value ?? string.Empty, " ", string.Empty), ',')
            };
        }

        protected override string PrepareInput(string input)
        {
            return input.TrimEnd();
        }
    }

    public class Program
    {
        public string Name;
        public int Weight;
        public string[] Programs;
    }
}
