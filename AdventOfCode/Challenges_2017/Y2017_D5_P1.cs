﻿using AdventOfCode.Base;
using AdventOfCode.Utils;

namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D5_P1 : AChallenge
    {
        public static Y2017_D5_P1 Build() { return new Y2017_D5_P1(); }

        //https://adventofcode.com/2017/day/5
        public override string Solve(string Input)
        {
            int output = 0;

            Y2017_D5_State state = new Y2017_D5_State
            {
                Items = UsefullTools.GetOneDimensionalArray<int>(Input, '\n'),
                Index = 0
            };

            for (int i=state.Index; i < state.Items.Length; ++output)
            {
                ChangeValues(state);
                i = state.Index;
            }

            return output.ToString();
        }

        protected virtual void ChangeValues(Y2017_D5_State state)
        {
            state.Items[state.Index]++;
            state.Index += state.Items[state.Index] - 1;
        }

        protected override string PrepareInput(string input)
        {
            return input.TrimEnd();
        }

    } 
}

// support classes
public class Y2017_D5_State
{
    public int[] Items;
    public int Index;
}
