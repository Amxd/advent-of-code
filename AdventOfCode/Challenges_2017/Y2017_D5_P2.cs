﻿namespace AdventOfCode.Challenges_2017
{
    public class Y2017_D5_P2 : Y2017_D5_P1
    {
        public new static Y2017_D5_P2 Build() { return new Y2017_D5_P2(); }

        protected override void ChangeValues(Y2017_D5_State state)
        {
            int newIndex = state.Index + state.Items[state.Index];

            state.Items[state.Index] += (state.Items[state.Index] > 2 ? -1 : 1); 
            state.Index = newIndex;
        }
    }
}
