﻿using AdventOfCode.Base;
using AdventOfCode.Utils;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace AdventOfCode
{
    class Program
    {
        // our statics
        public static string TOKEN = "53616c7465645f5f691dc6704596b2ae2c49f538733752cdaca1909e6b73125a5ef60eb8ba03dbe2c2cd0555c2dbce5a";
        public static string BUILD_METHOD_NAME = "Build";
        public static ChallengeRunner CRunner = ChallengeRunner.Build(TOKEN);

        static void Main(string[] args)
        {
            // run instances filtered by year
            CRunner.Run(GetInstances(2017, 7), (IChallenge challenge, string result) =>
            {
                Console.WriteLine($"{challenge.Year} Day{challenge.Day}{challenge.Part}");
                Console.WriteLine(result);
                Console.WriteLine();
            });

            Console.ReadKey();
        }

        // get our specific types from the assembly
        static IEnumerable<Type> GetTypes()
        {
            return Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(type => typeof(IChallenge).IsAssignableFrom(type));
        }

        // unfiltered
        static IEnumerable<IChallenge> GetInstances()
        {
            return GetTypes()
                .OrderBy(type => type.Name)
                .Select(type => (IChallenge)type.GetMethod(BUILD_METHOD_NAME).Invoke(null, null))
                .ToArray();
        }

        static IEnumerable<IChallenge> GetInstances(IEnumerable<Type> types)
        {
            return types
                .OrderBy(type => type.Name)
                .Select(type => (IChallenge)type.GetMethod(BUILD_METHOD_NAME).Invoke(null, null))
                .ToArray();
        }

        // filtered by year
        static IEnumerable<IChallenge> GetInstances(int year)
        {
            return GetInstances(GetTypes().Where(type => type.Name.StartsWith($"Y{year}")));
        }

        // filtered by year and day
        static IEnumerable<IChallenge> GetInstances(int year, int day)
        {
            return GetInstances(GetTypes().Where(type => type.Name.StartsWith($"Y{year}_D{day}")));
        }

        // filtered by year and day and Part
        static IEnumerable<IChallenge> GetInstances(int year, int day, ChallengePart part)
        {
            return GetInstances(GetTypes().Where(type => type.Name.Equals($"Y{year}_D{day}_{part}")));
        }
    }
}
