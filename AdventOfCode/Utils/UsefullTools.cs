﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Utils
{
    public class UsefullTools
    {
        public static T[][] GetBidimensionalArray<T>(string Input, char LineSeparator, char ItemSeparator)
        {
            var output = new List<T[]>();

            if(Input != string.Empty)
            {
                output.AddRange(Input.Split(LineSeparator).Select(row => GetOneDimensionalArray<T>(row, ItemSeparator)));
            }

            return output.ToArray();
        }

        public static T[] GetOneDimensionalArray<T>(string Input, char ItemSeparator)
        {
            var output = new List<T>();

            if (Input != string.Empty)
            {
                output.AddRange(Input.Split(ItemSeparator).Select(item => (T)Convert.ChangeType(item, typeof(T))));
            }

            return output.ToArray();
        }
    }
}
