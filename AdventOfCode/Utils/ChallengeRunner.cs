﻿using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Base;

namespace AdventOfCode.Utils
{
    public class ChallengeRunner
    {
        public static ChallengeRunner Build(string TOKEN)
        {
            return new ChallengeRunner(TOKEN);
        }

        public delegate void DisplayResult(IChallenge challenge, string result);

        private InputFetcher InputFetcher;
        private Dictionary<string, string> InputCache;

        public ChallengeRunner(string TOKEN)
        {
            InputFetcher = InputFetcher.Build(TOKEN);
            InputCache = new Dictionary<string, string>();
        }

        public void Run(IEnumerable<IChallenge> challenges, DisplayResult displayDelegate)
        {
            challenges.ToList().ForEach(challenge => Run(challenge, displayDelegate));
        }

        public void Run(IChallenge challenge, DisplayResult displayDelegate)
        {
            displayDelegate?.Invoke(challenge, challenge.Run(GetInput(challenge)));
        }

        private string GetInput(IChallenge challenge)
        {
            string inputID = $"{challenge.Year}|{challenge.Day}";

            if (!InputCache.ContainsKey(inputID))
            {
                InputCache.Add(inputID, InputFetcher.Get(challenge.Year, challenge.Day));
            }

            return InputCache[inputID];
        }
    }
}
