﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AdventOfCode.Utils
{
    public class InputFetcher
    {

        public static string BASE_URL = "https://adventofcode.com";
        public static string SESSION_TOKEN_TAG = "session";

        public static InputFetcher Build(string token)
        {
            return new InputFetcher(token);
        }

        private Uri BaseUri;
        private string Token;

        private InputFetcher(string token)
        {
            BaseUri = new Uri(BASE_URL);
            Token = token;
        }

        public string Get(int year, int day)
        {
            return _GetRemote(year, day).Result;
        }

        private async Task<string> _GetRemote(int year, int day)
        {
            var cookieContainer = new CookieContainer();
            cookieContainer.Add(BaseUri, new Cookie(SESSION_TOKEN_TAG, Token));

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            using (var client = new HttpClient(handler) { BaseAddress = BaseUri })
            {
                HttpResponseMessage message = await client.GetAsync($"{year}/day/{day}/input");
                return await message.Content.ReadAsStringAsync();
            }
        }
    }
}
