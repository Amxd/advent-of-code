﻿namespace AdventOfCode.Base
{
    public interface IChallenge
    {
        int Year { get; set; }
        int Day { get; set; }
        ChallengePart Part { get; set; }
        string Run(string input);
    }
}
