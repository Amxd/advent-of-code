﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Base
{
    public abstract class AChallenge : IChallenge
    {        
        public int Year { get; set; }
        public int Day { get; set; }
        public ChallengePart Part { get; set; }

        public AChallenge()
        {
            // Based on the Challenge Classes Naming Convention Y0000_D00_P0
            var match = Regex.Match(GetType().Name, @"Y([0-9]{4})_D([0-9]{1,2})_(P[1-2]{1})");

            // get the year
            Year = int.Parse(match.Groups[1].Value);

            // get the day
            Day = int.Parse(match.Groups[2].Value);

            // and get the enum that represents our P match
            Part = Enum
                .GetValues(typeof(ChallengePart))
                .Cast<ChallengePart>()
                .First(x => x.ToString() == match.Groups[3].Value);
        }

        // the abstract method mandatory for all challenges
        public abstract string Solve(string input);

        // the overridable method to fix the input
        protected virtual string PrepareInput(string input) { return input; }

        protected virtual void Init(string Input) { }

        // the run method mandatory by the IChallenge interface
        public string Run(string Input)
        {
            string PreparedInput = PrepareInput(Input);
            Init(PreparedInput);
            return Solve(PreparedInput);
        }
    }
}
